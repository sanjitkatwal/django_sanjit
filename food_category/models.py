from django.db import models


# Create your models here.
class Category(models.Model):
    title       = models.CharField(max_length=50, null=False, unique=False)
    description = models.TextField(null=True, default='This is description field.')
    status      = models.BooleanField(default=1)
    created_at  = models.DateTimeField(auto_now_add=True)
    updated_at  = models.DateTimeField(auto_now=True)
    created_by  = models.IntegerField(max_length=4, null=True)
    updated_by  = models.IntegerField(max_length=4, null=True)

    class Meta:
        db_table ="food_category"