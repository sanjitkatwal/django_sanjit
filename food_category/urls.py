from food_category import views
from django.urls import path


app_name = 'food-category'


urlpatterns = [
    path('/list', views.index, name='food_category_list'),
    path('/add-form', views.add_form, name='add_form'),
]