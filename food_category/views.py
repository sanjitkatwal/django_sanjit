from django.shortcuts import render
from food_category.models import Category
# Create your views here.


def index(request):
    category_list = Category.objects.all()
    return render(request, 'cms/food_category/index.html', context={'category_list': category_list})


def add_form(request):
    return render(request, 'cms/food_category/add_form.html')