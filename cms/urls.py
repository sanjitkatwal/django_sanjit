from cms import views
from django.urls import path


app_name = 'cms'


urlpatterns = [
    path('', views.dashboard, name='dashboard'),
]